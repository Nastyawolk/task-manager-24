package ru.t1.volkova.tm.command.system;

import org.jetbrains.annotations.NotNull;

public final class ApplicationAboutCommand extends AbstractSystemCommand {

    @NotNull
    private static final String ARGUMENT = "-a";

    @NotNull
    private static final String DESCRIPTION = "Show developer info.";

    @NotNull
    private static final String NAME = "about";

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("name: Anastasia Volkova");
        System.out.println("e-mail: aavolkova@t1-consulting.ru");
    }

    @Override
    public @NotNull String getArgument() {
        return ARGUMENT;
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }

}
